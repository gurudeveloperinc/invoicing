<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create-client','HomeController@getCreateClient');
Route::get('/create-invoice','HomeController@getCreateInvoice');
Route::get('/create-receipt','HomeController@getCreateReceipt');

Route::get('/view-clients','HomeController@getViewClients');
Route::get('/view-invoices','HomeController@getViewInvoices');
Route::get('/view-receipts','HomeController@getViewReceipts');


Route::post('/create-client','HomeController@postCreateClient');
Route::post('/create-invoice','HomeController@postCreateInvoice');
Route::post('/create-receipt','HomeController@postCreateReceipt');

