<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FirstDB extends Migration
{
	public function up()
	{
		//the client migration --  question please what does blueprint do? and again do we have to create different migration file for each table?
		Schema::create('clients',function(Blueprint $table){
			$table->increments('clid');
			$table->string('fname');
			$table->string('sname');
			$table->string('address',2000);
			$table->string('phone');
			$table->string('website')->nullable();
			$table->string('email');
			$table->string('companyName')->nullable();
			$table->softDeletes();
			$table->timestamps();

//            $table->unique('email');
//            $table->unique('phone');

		});

		Schema::create('invoices',function(Blueprint $table){
			$table->increments('invid');
			$table->integer('clid');
			$table->string('title');
			$table->decimal('total');
			$table->softDeletes();
			$table->timestamps();

		});

		Schema::create('invoice_details',function(Blueprint $table){
			$table->increments('indid');
			$table->integer('invid');
			$table->string('description',2000);
			$table->decimal('price');

			$table->softDeletes();
			$table->timestamps();

		});

		Schema::create('receipts',function(Blueprint $table){
			$table->increments('recid');
			$table->integer('invid');
			$table->decimal('amount');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('fname');
			$table->string('sname');
			$table->string('phone');
			$table->string('email');
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
		});


		Schema::create('password_resets', function (Blueprint $table) {
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at')->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('clients');
		Schema::dropIfExists('invoices');
		Schema::dropIfExists('invoice_details');
		Schema::dropIfExists('receipts');
		Schema::dropIfExists('password_resets');
		Schema::dropIfExists('users');



	}
}
