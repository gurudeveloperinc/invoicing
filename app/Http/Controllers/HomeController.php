<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

	public function getCreateClient() {

	}

	public function getCreateInvoice() {

	}

	public function getCreateReceipt() {

	}

	public function getViewClients() {

	}

	public function getViewInvoices() {

	}

	public function getViewReceipts() {

	}

	public function postCreateClient(Request $request) {

	}

	public function postCreateInvoice(Request $request) {

	}

	public function postCreateReceipt( Request $request ) {

	}
}
